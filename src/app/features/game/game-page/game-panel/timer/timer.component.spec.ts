import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimerComponent } from './timer.component';
import {GameDataService} from "../../../services/game-data/game-data.service";
import { ReactiveFormsModule} from "@angular/forms";
import {of} from "rxjs";
import {NO_ERRORS_SCHEMA} from "@angular/core";

describe(TimerComponent.name, () => {
  let component: TimerComponent;
  let fixture: ComponentFixture<TimerComponent>;
  let gameDataServiceMock: Partial<GameDataService>;

    beforeEach(() => {
    gameDataServiceMock = {
      isGameWasEnd$: of(false)
    };

    TestBed.configureTestingModule({
      declarations: [TimerComponent],
        providers: [
            { provide: GameDataService, useValue: gameDataServiceMock},
            ReactiveFormsModule
        ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(TimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe(TimerComponent.prototype.ngOnInit.name, ()=> {
    it('should subscribe to timerControl.valueChanges', () => {
      component.ngOnInit();
      component.timerControl.setValue(1000);
      component.timerControl.valueChanges.subscribe((value)=> {
        expect(value).toBe(1000);
      })
    });

    it('should reset timerControl after end of the game', () => {
      component.timerControl.patchValue = jasmine.createSpy();

      component.ngOnInit();

      expect(component.timerControl.patchValue).toHaveBeenCalledWith(null);
    });
  })
});
