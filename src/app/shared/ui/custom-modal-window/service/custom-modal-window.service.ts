import { Injectable } from '@angular/core';
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {CustomModalWindowComponent} from "../component/custom-modal-window.component";

@Injectable({
  providedIn: 'root'
})
export class CustomModalWindowService {
  constructor(private dialog: MatDialog) {}

  public openModal(config: MatDialogConfig): void {
   this.dialog.open(CustomModalWindowComponent, config);
  }
}
