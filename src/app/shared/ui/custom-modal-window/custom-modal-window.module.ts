import { NgModule } from '@angular/core';
import {CustomModalWindowComponent} from "./component/custom-modal-window.component";
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [
      CustomModalWindowComponent
  ],
  exports: [
    CustomModalWindowComponent
  ],
  imports: [
    MatDialogModule,
    MatButtonModule
  ]
})
export class CustomModalWindowModule { }
