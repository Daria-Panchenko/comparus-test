import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameComponent } from './game.component';
import {NO_ERRORS_SCHEMA} from "@angular/core";
import {GameDataService} from "../../services/game-data/game-data.service";
import {GameService} from "../../services/game/game.service";
import {of} from "rxjs";
import createSpy = jasmine.createSpy;
import {ColorsOfCells} from "../../models/game-enums";
import {Cell} from "../../models/game-interfaces";

describe(GameComponent.name, () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;
  let gameDataServiceMock: Partial<GameDataService>;
  let gameServiceMock: Partial<GameService>;

  beforeEach(() => {
      gameDataServiceMock = {
          isGameWasEnd$: of(true),
      };
      gameServiceMock = {
          unsubscribeStreams: createSpy('unsubscribeStreamsSpy'),
          winCellClicked: createSpy('winCellSpy'),
          startGame: createSpy('startGameSpy')
      };

      TestBed.configureTestingModule({
          declarations: [GameComponent],
          providers: [
            { provide: GameDataService, useValue: gameDataServiceMock},
            { provide: GameService, useValue: gameServiceMock},
          ],
          schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe(GameComponent.prototype.onClickCell.name, ()=> {
      it('should call winCell method with native element', () => {
          const cellToClickMock: Cell = {
              id: 2,
              color: ColorsOfCells.START_COLOR
          }

          component.onClickCell(cellToClickMock);

          expect(gameServiceMock.winCellClicked).toHaveBeenCalledWith(cellToClickMock);
      });
  });
});
