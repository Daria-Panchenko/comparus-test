import {ChangeDetectionStrategy, Component} from '@angular/core';
import {GameDataService} from "../../services/game-data/game-data.service";
import {combineLatest, Observable} from "rxjs";
import {JSON_DATA} from "../../models/game-config";
import {GameService} from "../../services/game/game.service";
import {PointsType} from "../../models/game-interfaces";

@Component({
  selector: 'app-game-panel',
  templateUrl: './game-panel.component.html',
  styleUrls: ['./game-panel.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GamePanelComponent {
    protected readonly JSON_DATA = JSON_DATA;
    public isButtonDisabled: Observable<boolean> = this.gameDataService.isButtonDisabled$;
    public points$: Observable<PointsType> = combineLatest({
      computerPoints: this.gameDataService.computerPoints$,
      userPoints: this.gameDataService.userPoints$
    });

    constructor(private gameDataService: GameDataService,
                private gameService: GameService) {
    }

    public onClickStartGame(): void {
        this.gameService.startGame();
        this.gameDataService.isButtonDisabled = true;
    }
}
