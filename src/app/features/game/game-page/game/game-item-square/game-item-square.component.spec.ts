import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameItemSquareComponent } from './game-item-square.component';

describe(GameItemSquareComponent.name, () => {
  let component: GameItemSquareComponent;
  let fixture: ComponentFixture<GameItemSquareComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GameItemSquareComponent]
    }).compileComponents();
    fixture = TestBed.createComponent(GameItemSquareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
