import {TestBed} from '@angular/core/testing';
import {GameService} from './game.service';
import {GameDataService} from "../game-data/game-data.service";
import {CustomModalWindowService} from "../../../../shared/ui/custom-modal-window/service/custom-modal-window.service";
import {ColorsOfCells} from "../../models/game-enums";
import {timer} from "rxjs";

describe(GameService.name, () => {
  let service: GameService;
  let gameDataServiceMock: Partial<GameDataService>;
  let modalServiceMock: Partial<CustomModalWindowService>;

  beforeEach(() => {
    gameDataServiceMock = {
      cells: [
          {
            id: 1,
            color: ColorsOfCells.START_COLOR
          },
        {
          id: 2,
          color: ColorsOfCells.START_COLOR
        }
      ]
    };

    modalServiceMock = {};

    TestBed.configureTestingModule({
      providers : [
          GameService,
        { provide: GameDataService, useValue: gameDataServiceMock },
        { provide: CustomModalWindowService, useValue: modalServiceMock },
      ]
    });
    service = TestBed.inject(GameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe(GameService.prototype.startGame.name, ()=> {
    it('should call methods for select cell', () => {
      service.startGame();
      expect(service['selectedCell']).toBeDefined();
    });
  });

  describe(GameService.prototype.winCellClicked.name, ()=> {
    let cell = {
      id: 1,
      color: ColorsOfCells.HIGHLIGHT_COLOR
    }

    it('should recolor cell', () => {
      gameDataServiceMock.cells = [
        {
          id: 1,
          color: ColorsOfCells.HIGHLIGHT_COLOR
        }
      ];

      service['selectedCell'] = 0;
      service.winCellClicked(cell);

      expect(gameDataServiceMock.cells[0].color).toBe(ColorsOfCells.WIN_COLOR);
    });

    it('should stop timer', () => {
      service['timerSubscription'] = timer(1).subscribe();
      let unsubscribeTimerSpy = spyOn(service['timerSubscription'], 'unsubscribe');

      service.winCellClicked(cell);

      expect(unsubscribeTimerSpy).toHaveBeenCalled();
    });
  });

  describe(GameService.prototype.unsubscribeStreams.name, ()=> {
    it('should unsubscribe timer steam', () => {
      service['timerSubscription'] = timer(1).subscribe();
      let unsubscribeTimerSpy = spyOn(service['timerSubscription'], 'unsubscribe');

      service.unsubscribeStreams();

      expect(unsubscribeTimerSpy).toHaveBeenCalled();
    });
  })
});
