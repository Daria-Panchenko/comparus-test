import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';
import {ColorsOfCells} from "../../features/game/models/game-enums";

@Directive({
  selector: '[appChooseColor]',
})
export class ChooseColorDirective implements OnInit {
  @Input({ required: true }) config!: { color: string };

  constructor(private el: ElementRef, private renderer: Renderer2) {
  }

  ngOnInit(): void {
    this.updateCell();
  }

  private updateCell(): void {
    this.el.nativeElement.style.backgroundColor = this.config.color;
    this.disableUsedCell();
  }

  private disableUsedCell (): void {
    let isCellWasUsed: boolean =
        this.config.color === ColorsOfCells.LOSE_COLOR ||
        this.config.color === ColorsOfCells.WIN_COLOR;

    if (isCellWasUsed) {
      this.renderer.addClass(this.el.nativeElement, 'disabled');
    }
  }
}
