import {AbstractControl, ValidatorFn} from "@angular/forms";
export interface CustomError {
    [key: string]: boolean;
}

export class CustomValidators {
    static timerValidator(): ValidatorFn {
        return (control: AbstractControl): CustomError | null => {
            let timerRgEx: RegExp = /^[^.,-]*$/;
            let valid:boolean = timerRgEx.test(control.value);

            if (!control.value || !valid) {
                return { positiveAndInteger: true };
            }

            return null;
        };
    }
}
