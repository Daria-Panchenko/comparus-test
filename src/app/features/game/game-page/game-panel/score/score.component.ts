import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScoreComponent {
  @Input({required:true}) computerPoints!:number;
  @Input({required:true}) userPoints!:number;
}
