import { TestBed } from '@angular/core/testing';
import { CustomModalWindowService } from './custom-modal-window.service';
import { MatDialog } from "@angular/material/dialog";
import { CustomModalWindowComponent } from "../component/custom-modal-window.component";
import { ModalConfig, ModalDataConfig } from "../models/custom-modal-window-interfaces";

const dataConfigMock: ModalDataConfig = {
  content : 'work',
  buttonContent: 'cancel'
}
const configMock: ModalConfig = {
  data : dataConfigMock
}

describe(CustomModalWindowService.name, () => {
  let service: CustomModalWindowService;
  let matDialogMock: Partial<MatDialog>;

  beforeEach(() => {
    matDialogMock = {
      open: jasmine.createSpy('openSpy')
    };

    TestBed.configureTestingModule({
      providers: [
          { provide: MatDialog, useValue: matDialogMock },
        CustomModalWindowService
      ],
    });

    service = TestBed.inject(CustomModalWindowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe(CustomModalWindowService.prototype.openModal.name, () => {
    it('should open a modal dialog with provided configuration', () => {
      service.openModal(configMock);

      expect(matDialogMock.open).toHaveBeenCalledWith(
          CustomModalWindowComponent,
          configMock
      );
    });
  });
});
