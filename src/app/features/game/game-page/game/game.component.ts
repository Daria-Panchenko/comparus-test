import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {filter, Observable, Subject, takeUntil, tap} from "rxjs";
import {GameService} from "../../services/game/game.service";
import {GameDataService} from "../../services/game-data/game-data.service";
import {Cell} from "../../models/game-interfaces";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameComponent implements OnDestroy {
  public cells$: Observable<Cell[]> = this.gameDataService.cells$;

  constructor(private gameService: GameService, private gameDataService: GameDataService) {
  }

  ngOnDestroy(): void {
    this.gameService.unsubscribeStreams();
  }

  public onClickCell (cell: Cell): void {
    this.gameService.winCellClicked(cell);
  }
}
