import { TestBed } from '@angular/core/testing';
import { GameDataService } from './game-data.service';
import {QUANTITY_OF_CELLS} from "../../models/game-constants";
import {ColorsOfCells} from "../../models/game-enums";
import {Cell} from "../../models/game-interfaces";

const cellsMock: Cell[] =  [
  {
    id: 1,
    color: ColorsOfCells.START_COLOR
  },
  {
    id: 2,
    color: ColorsOfCells.START_COLOR
  }
];

describe(GameDataService.name, () => {
  let service: GameDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[GameDataService]
    });

    service = TestBed.inject(GameDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('get startCells',()=> {
    it('should return an array of start cells with correct length and color', () => {
      const startCells = service.startCells;

      expect(startCells.length).toBe(QUANTITY_OF_CELLS);

      startCells.forEach((cell, index) => {
        expect(cell.color).toBe(ColorsOfCells.START_COLOR);
        expect(cell.id).toBe(index);
      });
    });
  });

  describe('get cells', ()=> {
    it('should return array of cells', () => {
      service['cells$$'].next(cellsMock);

      expect(service.cells).toBe(cellsMock);
    });
  });

  describe('set sells', ()=> {
    it('should set array of cells', () => {
      service.cells = cellsMock;

      expect(service.cells).toBe(cellsMock);
    });
  });

  describe('get computerPoints', ()=> {
    it('should return count of computerPoints', () => {
      const pointsMock: number = 5;
      service['computerPoints$$'].next(pointsMock);
      expect(service.computerPoints).toBe(pointsMock);
    });
  });

  describe('set computerPoints', ()=> {
    it('should set a number computerPoints', () => {
      const pointsMock: number = 5;
      service.computerPoints = pointsMock;
      expect(service.computerPoints).toBe(pointsMock);
    });
  });

  describe('get userPoints', ()=> {
    it('should return count of userPoints', () => {
      const pointsMock: number = 6;
      service['userPoints$$'].next(pointsMock);
      expect(service.userPoints).toBe(pointsMock);
    });
  });

  describe('set userPoints', ()=> {
    it('should set a number userPoints', () => {
      const pointsMock: number = 6;
      service.userPoints = pointsMock;
      expect(service.userPoints).toBe(pointsMock);
    });
  });

  describe('get timerValue', ()=> {
    it('should return timerValue', () => {
      const timerValueMock: number = 1000;
      service['_timerValue'] = timerValueMock;
      expect(service.timerValue).toBe(timerValueMock);
    });
  });

  describe('set timerValue', ()=> {
    it('should set timerValue', () => {
      const timerValueMock: number = 1000;
      service.timerValue = timerValueMock;
      expect(service.timerValue).toBe(timerValueMock);
    });
  });

  describe('set isButtonDisabled', ()=> {
    it('should set isButtonDisabled', () => {
      const isButtonDisabledMock: boolean = false;
      service.isButtonDisabled = isButtonDisabledMock;
      expect(service['isButtonDisabled$$'].getValue()).toBe(isButtonDisabledMock);
    });
  });

  describe(GameDataService.prototype.resetData.name, ()=> {
    it('should reset all data in gameDataService', () => {
      const isGameStartedSpy = spyOn(service['isGameWasEnd$$'], 'next');
      const cellsSpy = spyOn(service['cells$$'], 'next');
      const computerPointsSpy = spyOn(service['computerPoints$$'], 'next');
      const userPointsSpy = spyOn(service['userPoints$$'], 'next');
      const isButtonDisabledSpy = spyOn(service['isButtonDisabled$$'], 'next');

      service.resetData();

      expect(isGameStartedSpy).toHaveBeenCalledWith(false);
      expect(cellsSpy).toHaveBeenCalledWith(service.startCells);
      expect(computerPointsSpy).toHaveBeenCalledWith(0);
      expect(userPointsSpy).toHaveBeenCalledWith(0);
      expect(isButtonDisabledSpy).toHaveBeenCalledWith(false);
      expect(service['_timerValue']).toBe(0);
    });
  });
});
