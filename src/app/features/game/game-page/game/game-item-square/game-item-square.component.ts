import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-game-item-square',
  templateUrl: './game-item-square.component.html',
  styleUrls: ['./game-item-square.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameItemSquareComponent {

}
