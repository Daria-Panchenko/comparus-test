export interface Cell {
  id: number;
  color: string;
}

export interface PointsType {
  computerPoints: number,
  userPoints: number
}
