import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent {
  @Input() type: string = 'submit';
  @Input() disabled: boolean = false;
  @Output() onclickHandler: EventEmitter<Event> = new EventEmitter<Event>();

  public clickHandler($event: Event): void {
    this.onclickHandler.emit($event);
  }
}
