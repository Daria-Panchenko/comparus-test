import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomModalWindowComponent } from './custom-modal-window.component';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {ModalDataConfig} from "../models/custom-modal-window-interfaces";

const dataMock: ModalDataConfig  = {
  content: 'content',
  buttonContent: 'cancel'
}

describe(CustomModalWindowComponent.name, () => {
  let component: CustomModalWindowComponent;
  let fixture: ComponentFixture<CustomModalWindowComponent>;
  const matDialogMock: ModalDataConfig = dataMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
          CustomModalWindowComponent,
        { provide: MAT_DIALOG_DATA, useValue: matDialogMock },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(CustomModalWindowComponent);
    component = fixture.componentInstance;
  });

  it('should create component with injected data', () => {
    expect(component).toBeTruthy();
    expect(component.data).toEqual(dataMock);
  });
});
