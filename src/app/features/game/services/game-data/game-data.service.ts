import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {ColorsOfCells} from "../../models/game-enums";
import {Cell} from "../../models/game-interfaces";
import {QUANTITY_OF_CELLS} from "../../models/game-constants";

@Injectable({
  providedIn: 'root'
})
export class GameDataService {
  private isGameWasEnd$$: Subject<boolean> = new Subject<boolean>();
  private cells$$: BehaviorSubject<Cell[]> = new BehaviorSubject(this.startCells);
  private computerPoints$$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private userPoints$$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private _timerValue: number = 0;
  private isButtonDisabled$$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  public isGameWasEnd$: Observable<boolean> = this.isGameWasEnd$$.asObservable();
  public cells$:Observable<Cell[]> = this.cells$$.asObservable();
  public computerPoints$: Observable<number> = this.computerPoints$$.asObservable();
  public userPoints$: Observable<number> = this.userPoints$$.asObservable();
  public isButtonDisabled$: Observable<boolean> = this.isButtonDisabled$$.asObservable();

  public get startCells (): Cell[] {
    return  Array.from({ length: QUANTITY_OF_CELLS }, (_, i): Cell =>
        ({
          id: i,
          color: ColorsOfCells.START_COLOR
        }));
  }

  public get cells (): Cell[] {
    return this.cells$$.getValue();
  }

  public get computerPoints (): number {
    return this.computerPoints$$.getValue();
  }

  public get  userPoints (): number {
    return this.userPoints$$.getValue();
  }

  public get timerValue() : number {
    return this._timerValue;
  }

  public set computerPoints (points: number) {
    this.computerPoints$$.next(points);
  }

  public set userPoints (points: number) {
    this.userPoints$$.next(points);
  }

  public set cells (value: Cell[] ) {
    this.cells$$.next(value);
  }

  public set timerValue (time: number) {
    this._timerValue = time;
  }

  public set isButtonDisabled (isDisabled: boolean) {
    this.isButtonDisabled$$.next(isDisabled);
  }

  public resetData(): void {
    this.isGameWasEnd$$.next(false);
    this.computerPoints$$.next(0);
    this.userPoints$$.next(0);
    this.cells$$.next(this.startCells);
    this._timerValue = 0;
    this.isButtonDisabled$$.next(false);
  }
}
