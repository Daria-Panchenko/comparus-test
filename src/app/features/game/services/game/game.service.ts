import {Injectable} from '@angular/core';
import {Subscription, timer} from "rxjs";
import {ColorsOfCells, MODAL_CONTEXT} from "../../models/game-enums";
import {GameDataService} from "../game-data/game-data.service";
import {MAX_POINTS, MODAL_GAME_CONFIG, QUANTITY_OF_CELLS} from "../../models/game-constants";
import {Cell} from "../../models/game-interfaces";
import {CustomModalWindowService} from "../../../../shared/ui/custom-modal-window/service/custom-modal-window.service";
import {ModalConfig} from "../../../../shared/ui/custom-modal-window/models/custom-modal-window-interfaces";

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private availableCells: number[] = [...Array(QUANTITY_OF_CELLS).keys()];
  private selectedCell : number = -1;
  private timerSubscription!: Subscription;

  private get isGameOver (): boolean {
    return this.gameDataService.computerPoints === MAX_POINTS ||
        this.gameDataService.userPoints === MAX_POINTS;
  }

  private get modalConfig ():ModalConfig {
    const config: ModalConfig = MODAL_GAME_CONFIG;
    const isUserWin: boolean = this.gameDataService.userPoints === 10;

    config.data.content = isUserWin ? MODAL_CONTEXT.WIN : MODAL_CONTEXT.LOSE;

    return config;
  }

  constructor(private gameDataService: GameDataService,
              private modalWindowService: CustomModalWindowService) {
  }

  public startGame(): void {
    this.selectYellowField();
    this.startTimer();
  }

  public unsubscribeStreams (): void {
    this.timerSubscription.unsubscribe();
  }

  public winCellClicked(cell: Cell): void {
    if (cell.color === ColorsOfCells.HIGHLIGHT_COLOR) {
      this.stopTimer();
      this.changeColorOfCell(ColorsOfCells.WIN_COLOR);
      this.gameDataService.userPoints = ++this.gameDataService.userPoints;
      this.continueGame();
    }
  }

  private startTimer(): void  {
    this.timerSubscription = timer(this.gameDataService.timerValue)
        .subscribe((): void => {
          this.changeColorOfCell(ColorsOfCells.LOSE_COLOR);
          this.gameDataService.computerPoints = ++this.gameDataService.computerPoints;
          this.continueGame();
        });
  }

  private stopTimer(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  private changeColorOfCell (newColor: string): void {
    let cells: Cell[] = this.gameDataService.cells;

    cells[this.selectedCell] = {
      id: this.selectedCell,
      color: newColor
    };

    this.gameDataService.cells = cells;
  }

  private continueGame (): void {
    this.isGameOver ? this.endGame() : this.startGame();
  }

  private endGame(): void {
    this.openModalWindow();
    this.gameDataService.resetData();
    this.selectedCell = -1;
    this.availableCells = [...Array(QUANTITY_OF_CELLS).keys()];
  }

  private openModalWindow (): void {
    this.modalWindowService.openModal(this.modalConfig);
  }

  private arrayRandElement(arr: number[]): number {
    return Math.floor(Math.random() * arr.length);
  }

  private selectYellowField (): void {
    const indexOfCell: number = this.arrayRandElement(this.availableCells);

    this.selectedCell = this.availableCells[indexOfCell];
    this.availableCells.splice(indexOfCell,1);

    this.changeColorOfCell(ColorsOfCells.HIGHLIGHT_COLOR);
  }
}
