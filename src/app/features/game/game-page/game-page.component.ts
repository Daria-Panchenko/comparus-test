import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-game-page',
  templateUrl: './game-page.component.html',
  styleUrls: ['./game-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GamePageComponent {
}
