import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ButtonComponent } from './button.component';
import {By} from "@angular/platform-browser";

describe(ButtonComponent.name, () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ButtonComponent]
    }).compileComponents();
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set default input values', () => {
    expect(component.type).toBe('submit');
    expect(component.disabled).toBe(false);
  });

  describe('clickHandler', ()=>{
    it('should emit click event when button is clicked', () => {
      const button = fixture.debugElement.query(By.css('button'));
      spyOn(component.onclickHandler, 'emit');

      button.nativeElement.click();

      expect(component.onclickHandler.emit).toHaveBeenCalledWith(jasmine.any(Event));
    });
  })
});
