import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GamePanelComponent } from './game-panel.component';
import {GameDataService} from "../../services/game-data/game-data.service";
import {GameService} from "../../services/game/game.service";
import createSpy = jasmine.createSpy;

describe(GamePanelComponent.name, () => {
  let component: GamePanelComponent;
  let fixture: ComponentFixture<GamePanelComponent>;

  let gameDataServiceMock: Partial<GameDataService>;
  let gameServiceMock: Partial<GameService>;

  beforeEach(() => {
    gameDataServiceMock = {
    };
    gameServiceMock = {
      startGame: createSpy('startGameSpy')
    };

    TestBed.configureTestingModule({
      declarations: [GamePanelComponent],
      providers: [
          { provide: GameDataService, useValue: gameDataServiceMock },
        { provide: GameService, useValue: gameServiceMock},
      ]
    })
        .overrideComponent(GamePanelComponent, {
          set: {
            template: ''
          }
        })
        .compileComponents();

    fixture = TestBed.createComponent(GamePanelComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe(GamePanelComponent.prototype.onClickStartGame.name, ()=>{
    it('should call method startGame', ()=> {
      component.onClickStartGame();
      expect(gameServiceMock.startGame).toHaveBeenCalled();
    })
    it('should call set disabled to button', ()=> {
      component.onClickStartGame();
      expect(gameDataServiceMock.isButtonDisabled).toBe(true);
    })
  })
});
