import {ModalConfig} from "../../../shared/ui/custom-modal-window/models/custom-modal-window-interfaces";
import {JSON_DATA} from "./game-config";

export const QUANTITY_OF_CELLS: number = 25;
export const MAX_POINTS: number = 10
export const MODAL_GAME_CONFIG: ModalConfig = {
    width: '300px',
    data: {
        content: '',
        buttonContent: JSON_DATA.modalWindow.cancelButton
    }
}
