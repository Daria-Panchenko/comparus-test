import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";
import {GameDataService} from "../../../services/game-data/game-data.service";
import {debounceTime, distinctUntilChanged, Subject, takeUntil} from "rxjs";
import {CustomValidators} from "../../../../../shared/validators/custom.validators";
import {JSON_DATA} from "../../../models/game-config";

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimerComponent implements OnInit, OnDestroy {
  protected readonly JSON_DATA = JSON_DATA;
  private destroyed$: Subject<void> = new Subject();

  public timerControl: FormControl = new FormControl(null, {
      validators: [
          CustomValidators.timerValidator()
      ]
  });

  constructor(private gameDataService: GameDataService) {
  }

  ngOnInit(): void {
    this.checkTimerControl();
    this.checkIsGameWasEnd();
  }

  ngOnDestroy(): void {
      this.destroyed$.next();
      this.destroyed$.complete();
  }

  private checkTimerControl (): void {
      this.timerControl.valueChanges
          .pipe(
              takeUntil(this.destroyed$),
              debounceTime(500),
              distinctUntilChanged()
          )
          .subscribe((value) => this.setTimerValue(value));
  }

  private checkIsGameWasEnd (): void {
      this.gameDataService.isGameWasEnd$
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => this.resetTimerControl());
  }

  private resetTimerControl (): void {
      this.timerControl.patchValue(null);
  }

  private setTimerValue (value: number): void {
      this.gameDataService.isButtonDisabled = this.timerControl.invalid;
      this.gameDataService.timerValue = value;
  }
}
