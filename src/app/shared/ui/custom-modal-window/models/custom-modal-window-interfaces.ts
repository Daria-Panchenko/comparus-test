export interface ModalDataConfig {
    content: string;
    buttonContent: string;
}

export interface ModalConfig {
    data: ModalDataConfig;
    width?: string;
}
