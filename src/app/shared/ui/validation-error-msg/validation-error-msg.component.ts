import {AfterContentChecked, ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {FormControl} from "@angular/forms";
import {ERROR_MSG} from "../custom-modal-window/models/validation-errors-msg";

@Component({
  selector: 'app-validation-error-msg',
  templateUrl: './validation-error-msg.component.html',
  styleUrls: ['./validation-error-msg.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ValidationErrorMsgComponent implements AfterContentChecked {
  @Input({required: true}) control!: FormControl;
  public errorMessage: any = ERROR_MSG;
  public oneError: string = '';

  ngAfterContentChecked(): void {
    if(this.control?.errors) {
      this.oneError = Object.keys(this.control.errors)[0];
    }
  }
}
