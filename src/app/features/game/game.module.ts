import {ChangeDetectorRef, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GamePageComponent} from "./game-page/game-page.component";
import {GameRoutingModule} from "./game-routing.module";
import { GamePanelComponent } from './game-page/game-panel/game-panel.component';
import { ScoreComponent } from './game-page/game-panel/score/score.component';
import {SharedModule} from "../../shared/shared.module";
import { TimerComponent } from './game-page/game-panel/timer/timer.component';
import { GameComponent } from './game-page/game/game.component';
import { GameItemSquareComponent } from './game-page/game/game-item-square/game-item-square.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDialog, MatDialogModule} from "@angular/material/dialog";

@NgModule({
  declarations: [
    GamePageComponent,
    GamePanelComponent,
    ScoreComponent,
    TimerComponent,
    GameComponent,
    GameItemSquareComponent
  ],
  imports: [
    CommonModule,
    GameRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class GameModule {
}
