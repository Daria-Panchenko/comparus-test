import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidationErrorMsgComponent } from './validation-error-msg.component';
import { FormControl, ReactiveFormsModule } from "@angular/forms";
import {CommonModule} from "@angular/common";

describe(ValidationErrorMsgComponent.name, () => {
  let component: ValidationErrorMsgComponent;
  let fixture: ComponentFixture<ValidationErrorMsgComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ValidationErrorMsgComponent],
      imports: [CommonModule, ReactiveFormsModule]
    }).compileComponents();
    fixture = TestBed.createComponent(ValidationErrorMsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe(ValidationErrorMsgComponent.prototype.ngAfterContentChecked.name, ()=>{
    it('should update oneError when control has errors', () => {
      component.control = new FormControl('');
      component.control.setErrors({
        required : true,
      })

      fixture.detectChanges();
      expect(component.oneError).toBe('required');
    });

    it('should not update oneError when control has no errors', () => {
      component.control = new FormControl('');
      fixture.detectChanges();

      expect(component.oneError).toBe('');
    });
  })
});
