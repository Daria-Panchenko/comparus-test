import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonComponent} from "./ui/button/button.component";
import { ValidationErrorMsgComponent } from './ui/validation-error-msg/validation-error-msg.component';
import { ChooseColorDirective } from './directives/choose-color.directive';
import {CustomModalWindowModule} from "./ui/custom-modal-window/custom-modal-window.module";

@NgModule({
  declarations: [
    ButtonComponent,
    ValidationErrorMsgComponent,
    ChooseColorDirective,
  ],
    exports: [
        ButtonComponent,
        ChooseColorDirective,
        ValidationErrorMsgComponent,
        CustomModalWindowModule
    ],
  imports: [
    CommonModule
  ]
})
export class SharedModule {
}
