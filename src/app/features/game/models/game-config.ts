export const JSON_DATA: any = {
    "buttonStartGame": "Почати",
    "timer": {
        "placeholder": "час у мілісекундах",
        "label": "N = "
    },
    "modalWindow": {
        "cancelButton": "Закрити"
    }
}
