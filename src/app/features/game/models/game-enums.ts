export enum ColorsOfCells {
    LOSE_COLOR = '#E42626',
    START_COLOR = '#408DFF',
    WIN_COLOR = '#31B536',
    HIGHLIGHT_COLOR = '#FFFF25',
}

export enum MODAL_CONTEXT {
    WIN = 'Перемога!',
    LOSE = 'Програш('
}
